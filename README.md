# pyTasker
Simple tasking of python scripts in Windows

## Download
Releases can be found [here](https://gitlab.com/wabosh/pytasker/tags)

## Installation
- Make sure Python >3.6 is installed
- Place the content of this inside any directory
- Optional: If you want to use it system-wide, add its path to PATH
