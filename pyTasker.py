from __future__ import unicode_literals

import os
import re
import subprocess
import time
import getpass

from prompt_toolkit import prompt
from prompt_toolkit import PromptSession
from prompt_toolkit.completion import WordCompleter
from prompt_toolkit.shortcuts import ProgressBar
from prompt_toolkit.styles import Style

__version__ = "v0.1.1"

taskPrefix = r"pyts_"
taskDirectory = os.path.join(os.path.dirname(os.path.realpath(__file__)), "scripts")

titletext = r'''
                    /########                  /##                          
                   |__  ##__/                 | ##                          
    /######  /##   /##| ##  /######   /#######| ##   /##  /######   /###### 
   /##__  ##| ##  | ##| ## |____  ## /##_____/| ##  /##/ /##__  ## /##__  ##
  | ##  \ ##| ##  | ##| ##  /#######|  ###### | ######/ | ########| ##  \__/
  | ##  | ##| ##  | ##| ## /##__  ## \____  ##| ##_  ## | ##_____/| ##      
  | #######/|  #######| ##|  ####### /#######/| ## \  ##|  #######| ##      
  | ##____/  \____  ##|__/ \_______/|_______/ |__/  \__/ \_______/|__/      
  | ##       /##  | ##                                                      
  | ##      |  ######/   {version}                                          
  |__/       \______/                                                       

'''.format(version=__version__)

helptext = '''
Command         Description
==============  ===========================
tasks           Prints all tasks created
                using this tool
scripts         Lists all available scripts
                in ./scripts
delete <name>   Deletes the <name> task
create          Creates a daily task. Type
                "help create" for further 
                help
run <name>      Runs the task <name>
                immediately
help            Prints this help
exit            Quits the CLI
'''

createhelptext = '''
Usage: create <scriptname> [-a "<taskname>" "[arg1]" "[arg2]"]
'''

def f_list_current():
    # Gets all tasks
    tasks = subprocess.check_output(["schtasks.exe"]).decode("ascii")
    tasks = tasks.replace("\r", "")
    # Filters for tasks with taskPrefix
    tasks = re.findall(r".*" + taskPrefix + r".*", tasks)

    if len(tasks) == 0:
        print("No tasks found")
    else:
        # "Title" output
        print("")
        desc_line = "TaskName"
        for _ in range(33 - len(taskPrefix)):
            desc_line += " "
        desc_line += "Next Run Time          Status"
        print(desc_line)
        sep_line = ""
        for _ in range(40 - len(taskPrefix)):
            sep_line += "="
        sep_line += " ====================== ==============="
        print(sep_line)
    for t in tasks:
        # Print each task with taskPrefix removed
        t = t.replace(taskPrefix, "", 1)
        print(t)
    print("")


def f_list_available():
    scripts = [f for f in os.listdir(taskDirectory) if os.path.isfile(os.path.join(taskDirectory, f)) & f.endswith(".py")]    
    if len(scripts) == 0:
        print("No scripts found")
        return
    
    titlelength = 10
    filelength = 10
    paramlength = 10
    tuples = []
    for s in scripts:
        (t, _, p) = p_read_metadata(os.path.join(taskDirectory, s))
        if len(t) > titlelength:
            titlelength = len(t)
        if len(p) > paramlength:
            paramlength = len(p)
        if len(s) > filelength:
            filelength = len(s)

        tuples.append((t, s, p))

    p_print_table(3, ("Title", "Filename", "Parameters"), (titlelength, filelength, paramlength), "=", tuples)


def f_delete_task(filename):
    filename = filename.replace(".py", "")
    if p_taskname_exist(filename):
        try:
            out = subprocess.check_output(["schtasks.exe", "/Delete", "/TN", taskPrefix + filename, "/F"]).decode("ascii").replace("\r", "")
        except subprocess.CalledProcessError:
            print("You must start this tool with administration permission")
        else:
            out = out.replace(taskPrefix, "")
            print(out)
    else:
        print("Task not found")


def f_create_task(args):
    # Split arguments from filename
    res = re.search(r"(?<=-a ).*", args)
    taskname = args
    scriptname = args
    argslist = ""
    if res:
        res = res.group(0)
        scriptname = args.replace(res, "").replace("-a", "").strip()
        args = [x for x in re.findall(r"(?<=\").*?(?=\")", res) if x != " "]
        if len(args) == 0:
            print("-a needs at least one argument for the taskname")
            return
        taskname = args[0]
        for a in args[1:]:
            argslist += a + " "
        argslist = argslist.strip()

    if not p_taskname_exist(taskname):
        if p_scriptname_exists(scriptname):
            try:
                out = subprocess.check_output([
                    "schtasks.exe",
                    "/CREATE",
                    "/TN", taskPrefix + taskname,
                    "/NP",
                    "/TR", "python.exe " + os.path.join(taskDirectory, scriptname + ".py") + " " + argslist,
                    "/SC", "DAILY"
                ]).decode("ascii").replace("\r", "")
            except subprocess.CalledProcessError:
                pass
            else:
                out = out.replace(taskPrefix, "")
                print(out)
        else:
            print("Script \""+ scriptname + "\" does not exist")
    else:
        print("Task already exists")
    pass


def f_run_task(taskname):
    if p_taskname_exist(taskname):
        try:
            out = subprocess.check_output([
                "schtasks.exe",
                "/RUN",
                "/TN", taskPrefix + taskname
            ]).decode("ascii").replace("\r", "")
        except subprocess.CalledProcessError:
            pass
        else:
            out = out.replace(taskPrefix, "")
            print(out)


def p_read_metadata(file):
    title = ""
    description = ""
    parameters = ""
    with open(file, "r") as f:
        data = f.read()

        # Read title
        t_title = re.search(r"(?<=Title: ).*", data)
        if type(t_title) is re.Match:
            title = t_title.group()
        
        # Read Description
        t_description = re.search(r"(?<=Description: ).*", data)
        if type(t_description) is re.Match:
            description = t_description.group()

        # Read Parameters
        t_parameters = re.search(r"(?<=Parameters: ).*", data)
        if type(t_parameters) is re.Match:
            parameters = t_parameters.group()


    return (title, description, parameters)


def p_print_table(columns, headers, headerwidths, hline="=", entries=[]):
    assert len(headers) == columns
    assert len(headerwidths) == columns

    lst = list(headerwidths)
    for i in range(len(lst)):
        lst[i] += 1
    headerwidths = tuple(lst)

    headline = ""
    for i in range(columns):
        h = headers[i]
        headline += h
        for _ in range(headerwidths[i] - len(h)):
            headline += " "
        headline += " "

    line = ""
    for i in range(columns):
        for _ in range(headerwidths[i]):
            line += hline
        line += " "

    print()
    print(headline)
    print(line)

    for a in entries:
        assert len(a) == columns
        argLine = ""
        for j in range(columns):
            aCol = a[j]
            argLine += aCol
            for _ in range(headerwidths[j] - len(aCol)):
                argLine += " "
            argLine += " "
        print(argLine)

    print()


def p_taskname_exist(taskname):
    tasks = subprocess.check_output(["schtasks.exe"]).decode("ascii")
    tasks = tasks.replace("\r", "")
    tasks = re.findall(taskPrefix + taskname, tasks)
    return len(tasks) > 0


def p_scriptname_exists(scriptname):
    scripts = [f for f in os.listdir(taskDirectory) if os.path.isfile(os.path.join(taskDirectory, f)) & f.endswith(".py")]
    return scriptname + ".py" in scripts


def main():
    session = PromptSession()
    print(titletext)
    while True:
        try:
            text = session.prompt("> ")
        except KeyboardInterrupt:
            break
        except EOFError:
            break
        else:
            text = text.strip()
            if text == "exit":
                break
            elif text == "tasks":
                f_list_current()
            elif text == "scripts":
                f_list_available()
            elif text == "help":
                print(helptext)
            elif text.startswith("delete"):
                text = text.strip()
                text = text.replace("delete", "", 1)
                if text.startswith(" "):
                    f_delete_task(text.replace(" ", "", 1))
                else:
                    print("You must enter a taskname")
            elif text.startswith("create"):
                text = text.strip()
                text = text.replace("create", "", 1)
                if text.startswith(" "):
                    f_create_task(text.replace(" ", "", 1))
                else:
                    print("You must enter a taskname")
            elif text.startswith("run"):
                text = text.strip()
                text = text.replace("run", "", 1)
                if text.startswith(" "):
                    f_run_task(text.replace(" ", "", 1))
                else:
                    print("You must enter a taskname")
            elif text == "help create":
                print(createhelptext)
            elif text == "":
                continue
            else:
                print("Command not found: " + text)


if __name__ == "__main__":
    main()
