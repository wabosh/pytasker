'''
Title: Directory Cleanup
Description: Clears a directories files given on their age
Parameters: <Path> <Age; e.g. 3D, 1W, 4M>
'''

import sys
import os
from shutil import rmtree
import time
from datetime import datetime, timedelta
from pytimeparse.timeparse import timeparse

def checkUserError(condition, message):
    if condition == False:
        exit(message)

checkUserError(len(sys.argv) == 3, "Usage: python cleanup.py <directory> <age>")

c_dir = sys.argv[1]

checkUserError(os.path.isdir(c_dir), "First argument must be a valid directory")

maximum_file_age = datetime.now() - timedelta(seconds=timeparse(sys.argv[2]))

# Create a list of all files to delete
files_to_delete = []
for file in os.scandir(c_dir):
    change_date = datetime.fromtimestamp(os.path.getmtime(file.path))
    if (change_date < maximum_file_age):
        files_to_delete.append(file)

# Delete those files
for file in files_to_delete:
    try:
        if os.path.isdir(file.path):
            rmtree(file.path)
        else:
            os.remove(file.path)
    except PermissionError:
        pass
